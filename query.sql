select distinct a.first_name, a.middle_name, a.last_name, b.title, p.name
from authors a
left join book_authors ba on ba.author_id   = a.author_id
left join books b 		  on b.book_id      = ba.book_id
left join publishers p    on p.publisher_id = b.publisher_id
left join book_genres bg  on bg.book_id     = b.book_id
left join genres g 		  on g.genre_id 	= bg.genre_id
order by g.genre; 